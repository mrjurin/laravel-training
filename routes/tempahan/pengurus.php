<?php
use Illuminate\Support\Facades\Route;

Route::prefix('pengurus-tempahan')->group(function () {

    Route::get('/senarai-tempahan',function(){
        echo '<h1>Senarai Tempahan</h1>';
    });

    Route::put('/lulus',function(){
        echo '<h1>Back Process Kelulusan</h1>';
    });
    
    Route::put('/tolak',function(){
        echo '<h1>Back Process Tolak</h1>';
    });
});