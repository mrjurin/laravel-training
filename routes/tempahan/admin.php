<?php

use App\Http\Controllers\BookingController;
use Illuminate\Support\Facades\Route;
Route::middleware('auth:sanctum')->prefix('admin-tempahan')->group(function () {
    Route::get('/senarai',[BookingController::class,'senaraiBilik'])->name('senarai.bilik');
    Route::get('/tambah',[BookingController::class,'tambahBilik'])->name('tambah.bilik');    
    Route::post('/simpan',[BookingController::class,'save'])->name('simpan.bilik');
    Route::get('/sunting/{id}',[BookingController::class,'edit'])->name('sunting.bilik');
    Route::put('/update/{id}',[BookingController::class,'update'])->name('update.bilik');
    Route::delete('/hapus/{id}',[BookingController::class,'delete'])->name('hapus.bilik');
});
// Route::get('/admin-tempahan/tambah',[BookingController::class,'tambahBilik'])->name('tambah.bilik');