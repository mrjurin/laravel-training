<?php

use App\Http\Controllers\LoginController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;

include('tempahan/admin.php');
include('tempahan/pengurus.php');
include('tempahan/penempah.php');

Route::get('/', function () {
    return view('welcome');
});

Route::get('/log-masuk',[LoginController::class,'login'])->name('logmasuk');
Route::post('/authenticate',[LoginController::class,'authenticate'])->name('periksa.pengguna');
Route::post('/lupa-katalaluan',[LoginController::class,'forgotPassword'])->name('lupa.katalaluan');
Route::post('/log-keluar',[LoginController::class,'logout'])->name('logkeluar');


Route::middleware('auth:sanctum')->resource('admin-user',UserController::class);
