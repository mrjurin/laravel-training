<?php

namespace Database\Seeders;

use App\Models\Lookup;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class LookupSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        // DB::table('lookups')->where('cat','ROOMSTS')->delete();
        Lookup::where('cat','ROOMSTS')->delete();
        $arr = [
            ['cat' => 'ROOMSTS','code' => '1','descr' => 'Kosong'],
            ['cat' => 'ROOMSTS','code' => '2','descr' => 'Telah Ditempah'],
            ['cat' => 'ROOMSTS','code' => '3','descr' => 'Diubahsuai'],
            ['cat' => 'ROOMSTS','code' => '4','descr' => 'Rosak'],
        ];
        DB::table('lookups')->insert($arr);

        Lookup::where('cat','GENDER')->delete();
        $arr = [
            ['cat' => 'GENDER','code' => 'M','descr' => 'Lelaki'],
            ['cat' => 'GENDER','code' => 'F','descr' => 'Perempuan'],
        ];
        DB::table('lookups')->insert($arr);
        
        Lookup::where('cat','COURSE')->delete();
        $arr = [
            ['cat' => 'COURSE','code' => 'F01','descr' => 'Sijil Teknologi Komputer Sistem'],
            ['cat' => 'COURSE','code' => 'F02','descr' => 'Sijil Teknologi Komputer Rangkaian'],
        ];
        DB::table('lookups')->insert($arr);

    }
}
