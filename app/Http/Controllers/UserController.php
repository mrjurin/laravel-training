<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(Request $req)
    {
        $data['users']=User::paginate(20);
        $data['name'] = $req->name;
        return view('admin.senarai',$data);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $data['user']= new User();
        return view('admin.borang',$data);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $req)
    {
        $req->validate([
            'name'=>'required',
            'email'=>'required|email',
            'password'=>'required|confirmed',
        ]);

        $user = new User();
        $user->name = $req->name;
        $user->email = $req->email;
        $user->password = Hash::make($req->password);
        $user->save();

        return redirect()->route('admin-user.index')->with('success','Rekod berjaya diwujudkan');
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        $data['user'] = User::find($id);        

        return view('admin.borang',$data);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $req, string $id)
    {
        
        
        $req->validate([
            'name'=>'required',
            'email'=>'required|email',
            'password'=>'required|confirmed',
        ]);
        $user = User::find($id);        
        $user->name = $req->name;
        $user->email = $req->email;
        if (strlen($req->password)<15)
        $user->password = Hash::make($req->password);
        $user->save();

        return redirect()->route('admin-user.index')->with('success','Rekod berjaya dikemaskini');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $user = User::find($id);
        $user->delete();
        return redirect()->route('admin-user.index')->with('success','Rekod berjaya dihapuskan');
    }
}
