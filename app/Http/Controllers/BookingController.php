<?php

namespace App\Http\Controllers;

use App\Models\Lookup;
use App\Models\Room;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;


class BookingController extends Controller
{
    // public $statusList = [
    //     '1'=>'Kosong',
    //     '2'=>'Telah Ditempah',
    //     '3'=>'Rosak',
    //     '4'=>'Renovasi'
    //     ];

    public function senaraiBilik(Request $req)
    {
        $room_name = $req->room_name;
        $room_status = $req->room_status;
        
        $data['room_name'] = $req->room_name;
        $data['room_status'] = $req->room_status;

        $data['username'] = Auth::user()->name;
        $data['statusList'] = Lookup::where('cat','ROOMSTS')->get();
        $data['courseList'] = Lookup::where('cat','COURSE')->get();

        $data['rooms'] = Room::when(!empty($room_name),function($q) use ($room_name){
            $q->where('room_name','like','%'.$room_name.'%');
        })->when(!empty($room_status),function($q) use ($room_status){
            $q->where('room_status',$room_status);
        })->paginate(2);  
        
        // $strsql = "SELECT * FROM rooms WHERE 1=1";
        // if (!empty($room_name)){
        //     $strsql  .= " AND room_name LIKE '%".$room_name."%'";
        // }

        // if (!empty($room_status)){
        //     $strsql  .= " AND room_status ='".$room_status."'";
        // }

        

        // $rooms = DB::select($strsql);


        // return view('tempahan.senarai',
        // compact('username','rooms','room_name','room_status','statusList'));
        
        return view('tempahan.senarai',$data);
    }

    /**
     * Show the form for creating a new bilik.
     */
    // public function create()
    public function tambahBilik()
    {
        $data['room'] = new Room();
        $data['statusList'] = Lookup::where('cat','ROOMSTS')->get();//$this->statusList;
        return view('tempahan.borang_tambah_bilik',$data);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function save(Request $req)
    {
        
        $validated = $req->validate([
            'room_name' => 'required|unique:App\Models\Room,room_name',
            'room_capacity' => 'required',
            'room_status' => 'required',
        ]);
        
        $bilik = new Room();
        $bilik->room_name = $req->room_name;
        $bilik->room_capacity = $req->room_capacity;
        $bilik->room_status = $req->room_status;
        $bilik->room_facility = $req->room_facility;
        $bilik->save();        
        
        return redirect(route('senarai.bilik'));
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        $data['room'] = Room::find($id);
        $data['statusList'] = Lookup::where('cat','ROOMSTS')->get();//$this->statusList;
        return view('tempahan.borang_tambah_bilik',$data);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $req, string $id)
    {
        // dd($req->lampiran);
        $validated = $req->validate([
            // 'room_name' => 'required',
            'room_name' => 'required|unique:App\Models\Room,room_name,'.$id,
            'room_capacity' => 'required',
            'room_status' => 'required',
        ]);
        
        $img = ['lampiran'=>[]];
        // foreach($req->lampiran as $m){

        // }

        $bilik = Room::find($id);
        $bilik->room_name = $req->room_name;
        $bilik->room_capacity = $req->room_capacity;
        $bilik->room_status = $req->room_status;
        $bilik->room_facility = $req->room_facility;
        $bilik->details = $img;
        $bilik->save();        
        
        return redirect(route('senarai.bilik'));
    }

    /**
     * Remove the specified resource from storage.
     */
    public function delete(string $id)
    {
        Room::find($id)->delete();
        return redirect(route('senarai.bilik'));
    }
}
