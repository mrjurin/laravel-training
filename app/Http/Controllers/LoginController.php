<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;


class LoginController extends Controller
{
    public function login(){
        return view('login.login');
    }

    public function logout(Request $req){
        Auth::logout(); 
        $req->session()->invalidate();
        $req->session()->regenerateToken();    
        return redirect('/');
    }

    public function authenticate(Request $req){
                $credentials = $req->validate([
            'email' => ['required', 'email'],
            'password' => ['required'],
        ]);
 
        if (Auth::attempt($credentials)) {
            $req->session()->regenerate();
 
            // return redirect()->intended('senarai.bilik');
            return redirect(route('senarai.bilik'));
        }
 
        return back()->withErrors([
            'email' => 'The provided credentials do not match our records.',
        ])->onlyInput('email');        
    }

    public function forgotPassword(){
        
    }
}
