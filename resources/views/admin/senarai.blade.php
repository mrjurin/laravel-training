@extends('layouts.master')
@section('title', 'Senarai Pengguna')
@section('content')
<h1>Pentadbiran Senarai Pengguna </h1>
@if (session('success'))
    <div class="alert alert-success">
        {{ session('success') }}
    </div>
@endif
<form action="{{route('admin-user.index')}}">
    @csrf
    <div class="mb-3">
    <label class="form-label">Nama Pengguna</label>
    <input type="text" name="name" value="{{$name}}" class="form-control">
    </div>

    <button type="submit" class="btn btn-primary">Cari</button>
    <a class="btn btn-warning" href="{{route('admin-user.index')}}">Set Semula</a>
</form>

<br>
<br>

<a class="btn btn-success" href="{{route('admin-user.create')}}">Tambah</a>


<br>
<table class="table table-striped">
    <tr>
        <th>Bil</th>
        <th>Nama Pengguna</th>
        <th>Emel</th>
        <th>Tindakan</th>
    </tr>
    @php
    $i = $users->firstItem();
    @endphp
    
   @foreach($users as $item)
   <tr>
        <td>{{$i++}}</td>
        <td>{{$item->name}}</td>
        <td>{{$item->email}}</td>
        <td>

            <a class="btn btn-info" href="{{route('admin-user.edit',$item->id)}}">Kemaskini</a>

            <form action="{{route('admin-user.destroy',$item->id)}}" method="POST">
                @method('delete')
                @csrf
                <button type="submit" onclick="return confirm('Anda pasti untuk hapus?')" 
                class="btn btn-danger">Hapus</button>
            </form>
            
        </td>
    </tr>
   @endforeach
</table>
{{ $users->appends(['name'=>$name])->links() }}
@endsection