@extends('layouts.master')
@section('title', 'Borang Tambah Pengguna')
@section('content')

@if(isset($user->id))
<h3>Borang Kemaskini Pengguna</h3>
@else
<h3>Borang Tambah Pengguna</h3>
@endif

@if (session('success'))
    <div class="alert alert-success">
        {{ session('success') }}
    </div>
@endif

@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif


@if(isset($user->id))
<form action="{{route('admin-user.update',$user->id)}}" method="POST">
  @method('PUT')
@else
<form action="{{route('admin-user.store')}}" method="POST">
@endif

  @csrf
  <div class="mb-3">
    <label class="form-label">Nama Pengguna</label>
    <input type="text" name="name" value="{{old('name',$user->name)}}" class="form-control">
  </div>

  <div class="mb-3">
    <label class="form-label">Emel</label>
    <input type="email" name="email" value="{{old('email',$user->email)}}" class="form-control">
  </div>

  <div class="mb-3">
    <label class="form-label">Katalaluan</label>
    <input type="password" name="password" value="{{old('password',$user->password)}}" class="form-control">
  </div>

  <div class="mb-3">
    <label class="form-label">Pengesahan Katalaluan</label>
    <input type="password" name="password_confirmation" value="{{old('password',$user->password)}}" class="form-control">
  </div>

  <button type="submit" class="btn btn-primary">Simpan</button>
  
  @if(isset($user->id))
  <a class="btn btn-warning" href="{{route('admin-user.edit',$user->id)}}">Set Semula</a>
  @else
  <a class="btn btn-warning" href="{{route('admin-user.create')}}">Set Semula</a>
  @endif
  
  <a class="btn btn-danger" href="{{route('admin-user.index')}}">Kembali</a>
</form>
@endsection