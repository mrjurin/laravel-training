<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Sistem Tempahan - @yield('title')</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/css/bootstrap.min.css" rel="stylesheet" 
    crossorigin="anonymous">
  </head>
  <body>
    
    

    <nav class="navbar navbar-expand-lg bg-body-tertiary">
  <div class="container-fluid">
    <a class="navbar-brand" href="/">ILP</a>
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <ul class="navbar-nav me-auto mb-2 mb-lg-0">
        <li class="nav-item">
          <a class="nav-link active" aria-current="page" href="#">Tempahan Bilik</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="/admin-user">Pengurusan Pengguna</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="{{route('admin-user.index')}}">Pengurusan Pengguna (Named)</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="/daftar-pengguna">Daftar Pengguna</a>
        </li>

        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
            Admin Tempahan
          </a>
          <ul class="dropdown-menu">
            <li><a class="dropdown-item" href="{{route('senarai.bilik')}}">Pengurusan Bilik</a></li>
            <li><a class="dropdown-item" href="#">Ubahsuai Bilik</a></li>
            <li><hr class="dropdown-divider"></li>
            <li><a class="dropdown-item" href="#">Aset Bilik</a></li>
          </ul>
        </li>
        @if(Auth::check())
        <li class="nav-item">          
          <form action="{{route('logkeluar')}}" method="POST">
            @csrf
          <button type="submit" class="nav-link">Log Keluar</button>
          </form>          
        </li>
        @endif
        
      </ul>
      
    </div>
  </div>
</nav>

    <div class="container">
        @yield('content')
    </div>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/js/bootstrap.bundle.min.js" 
    crossorigin="anonymous"></script>
  </body>
</html>