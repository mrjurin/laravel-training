@extends('layouts.master')
@section('title','Log Masuk')
@section('content')

@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<form action="{{route('periksa.pengguna')}}" method="POST">
    @csrf
    <div class="mb-3">
    <label class="form-label">ID Pengguna</label>
    <input type="text" name="email" placeholder="Sila masukkan ID Pengguna" class="form-control" required>
    </div>

    <div class="mb-3">
        <label class="form-label">Katalaluan</label>
        <input type="password" name="password" placeholder="Sila masukkan Katalaluan" class="form-control" required>
    </div>
    <button type="submit" class="btn btn-primary">Log Masuk</button>    
    <a class="btn btn-warning" href="{{route('logmasuk')}}">Set Semula</a>

    <a href="{{route('lupa.katalaluan')}}">Lupa Katalaluan</a>
</form>
@endsection