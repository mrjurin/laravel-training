@extends('layouts.master')
@section('title', 'Borang Tambah Bilik')
@section('content')

@if(isset($room->id))
<h3>Borang Kemaskini Bilik</h3>
@else
<h3>Borang Tambah Bilik</h3>
@endif

@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif


@if(isset($room->id))
<form enctype="multipart/form-data" action="{{route('update.bilik',$room->id)}}" method="POST" >
  @method('PUT')
@else
<form enctype="multipart/form-data" action="{{route('simpan.bilik')}}" method="POST">
@endif

  @csrf
  <div class="mb-3">
    <label class="form-label">Nama Bilik</label>
    <input type="text" name="room_name" value="{{old('room_name',$room->room_name)}}" class="form-control">
  </div>

  <div class="mb-3">
    <label class="form-label">Kapasiti Bilik</label>
    <input type="number" name="room_capacity" value="{{old('room_capacity',$room->room_capacity)}}" class="form-control">
  </div>

  <div class="mb-3">
      <label class="form-label">Status Bilik</label>
      <select class="form-select" name="room_status">
          <option value="">-- Sila Pilih --</option>
          @foreach($statusList as $m)
          
          <option value="{{$m->code}}" {{(old('room_status',$room->room_status) == $m->code ?'selected':'')}}>
                {{$m->descr}}
            </option>     
          @endforeach
      </select>
  </div>

  <div class="mb-3">
    <label class="form-label">Kemudahan</label>
    <textarea class="form-control" rows="3" name="room_facility">{{old('room_facility',$room->room_facility)}}</textarea>
  </div>

  <div class="mb-3">
    <label class="form-label">Lampiran</label>
    <input type="file" multiple name="lampiran[]" class="form-control">
  </div>


  <button type="submit" class="btn btn-primary">Simpan</button>
  <!-- <button type="reset" class="btn btn-warning">Set Semula</button> -->
  @if(isset($room->id))
  <a class="btn btn-warning" href="{{route('sunting.bilik',$room->id)}}">Set Semula</a>
  @else
  <a class="btn btn-warning" href="{{route('tambah.bilik')}}">Set Semula</a>
  @endif
  
  <a class="btn btn-danger" href="{{route('senarai.bilik')}}">Kembali</a>
</form>
@endsection