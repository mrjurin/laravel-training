@extends('layouts.master')
@section('title', 'Senarai Bilik')
@section('content')
<h1>Pentadbiran Senarai Bilik : {{$username}}</h1>
<form action="{{route('senarai.bilik')}}">
    @csrf
    <div class="mb-3">
    <label class="form-label">Nama Bilik</label>
    <input type="text" name="room_name" value="{{$room_name}}" class="form-control">
    </div>

    <div class="mb-3">
        <label class="form-label">Status Bilik</label>
        <select class="form-select" name="room_status">
            <option value="">-- Sila Pilih --</option>
            @foreach($statusList as $m)
            <option value="{{$m->code}}" {{($room_status == $m->code ?'selected':'')}}>
                {{$m->descr}}
            </option>    
            @endforeach
        </select>
    </div>
    <button type="submit" class="btn btn-primary">Cari</button>
    <a class="btn btn-warning" href="{{route('senarai.bilik')}}">Set Semula</a>
</form>

<br>
<br>

<a class="btn btn-success" href="{{route('tambah.bilik')}}">Tambah</a>


<br>
<table class="table table-striped">
    <tr>
        <th>Bil</th>
        <th>Nama Bilik</th>
        <th>Kapasiti</th>
        <th>Status Bilik</th>
        <th>Kemudahan</th>
        <th>Tindakan</th>
    </tr>
    @php
    $i = $rooms->firstItem();
    @endphp
    
   @foreach($rooms as $item)
   <tr>
        <td>{{$i++}}</td>
        <td>{{$item->room_name}}</td>
        <td>{{$item->room_capacity}}</td>
        <td>{{$item->status->descr}}</td>
        <td>{{$item->room_facility}}</td>
        <td>

            <a class="btn btn-info" href="{{route('sunting.bilik',$item->id)}}">Kemaskini</a>
            <a class="btn btn-info" href="/admin-tempahan/sunting/{{$item->id}}">Kemaskini</a>

            <a class="btn btn-danger" 
            onclick="return confirm('Anda pasti untuk hapus?')" 
            href="/admin-tempahan/hapus/{{$item->id}}">Hapus Tak Betul</a>

            <form action="{{route('hapus.bilik',$item->id)}}" method="POST">
                @method('delete')
                @csrf
                <button type="submit" onclick="return confirm('Anda pasti untuk hapus?')" 
                class="btn btn-danger">Hapus</button>
            </form>
            
        </td>
    </tr>
   @endforeach
</table>
{{ $rooms->appends(['room_name'=>$room_name,'room_status'=>$room_status])->links() }}
@endsection