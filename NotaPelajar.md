# Installation & Tools

1. IDE - Visual Studio Code - alternative - NetBean, PhpStorm, Notepad, Sublime
2. Laragon - package inc. Apache Web Server & NGINX, MySQL (Heidi), PHP - IIS
3. Git - versioning tools
4. Node - package manager
5. Composer - 
6. SQLYog, Workbench, Aqua, DBeaver, Heidi  - phpMyAdmin
7. Bootstrap 

## Scenario - Sistem Tempahan Bilik Mesyuarat (STBM) - Web/ API

Asas - Utk Belajar
Log Masuk - Semua Pengguna / Semua Peranan - Authentication Function
Pengurusan Bilik Mesyuarat - Peranan Admin 
	- CRUD - Create Record, Delete Record, Update Record, Retrieve Record
Tempahan Saya - Peranan Penempah Bilik - CRUD
Senarai Tempahan - Pelulus Permohonan Tempahan - Terima/Tolak - CRUD

Asas Sistem
 - Audit trail

Variation Order (VO) - Change Request (CR)
 - Feedback Function - 

## Features 
 - Upload Function
 - Authentication Function
 - Email notification
 - Validation - mandatory fields
 - WA/SMS - Advance
 - Firebase Notification - Advance
 - Apply Theme - Layout
 - Generate PDF

## Technology -
 - Migration - 
 - Seeder

# Ubat

 - php artisan optimize:clear
 - php artisan list

# REST - get, post, delete, put

## Git
# git --version
# git clone --branch ILP-Selandar https://gitlab.com/isk6708/laravel-training.git

## User Factory

- php artisan tinker
- use \App\Models\User;  
- $m = new User();
- $m::factory(10)->create();  

## Assignment

Create / Finish function Daftar Pengguna (This one to cater public user)

- Public user will register with name, email and password.
- Once saved, please redirect public user to login screen



